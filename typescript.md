# Learning path: TypeScript

## How should I consume this list?

The :fire: items are better read in sequence, in the order they appear inside each section. The same applies for the
sections/topics: read in sequence. Everything else doesn't have a suggested order.

### Emoji labels

- :fire: Must read (required)
- :bulb: Use as a support material (read if/when necessary)
- :pushpin: Mostly optional, but important nonetheless
- :see_no_evil: Optional


## Clean code and good practices

1. :fire: [Clean Code concepts adapted for TypeScript](https://github.com/labs42io/clean-code-typescript)
1. :fire: [Defensive programming techniques explained with examples](https://www.golinuxcloud.com/defensive-programming)
1. :fire: [Five ways to code more defensively](https://codetrain.io/five-ways-to-code-defensively)


## Common design patterns in JS/TS

1. :fire: [Singleton and the module](https://wanago.io/2019/11/11/javascript-design-patterns-1-singleton-and-the-module)
1. :fire: [Factories](https://wanago.io/2019/12/02/javascript-design-patterns-factories-typescript)
1. :fire: [Facade](https://wanago.io/2019/12/09/javascript-design-patterns-facade-react-hooks)
1. :fire: [Decorators](https://wanago.io/2019/12/23/javascript-design-patterns-4-decorators-and-their-implementation-in-typescript)
1. :fire: [Observers](https://wanago.io/2020/01/20/javascript-design-patterns-observer-typescript)


## Object oriented programming

1. :fire: [Object-Oriented programming with TypeScript](https://dev.to/kevin_odongo35/object-oriented-programming-with-typescript-574o)
1. :fire: [Write Object-Oriented TypeScript well](https://blog.jetbrains.com/dotnet/2020/07/28/write-object-oriented-typescript-well)


## Functional programming

1. :fire: [Functional programming in JavaScript](https://opensource.com/article/17/6/functional-javascript)
1. :fire: [What are side effects, and what you can do about them](https://thejs.dev/jmitchell/what-are-side-effects-and-what-you-can-do-about-them-jws)
1. :fire: [How to write a more declarative TypeScript Code?](https://kkalamarski.me/how-to-write-a-more-declarative-typescript-code-maybe-monad-implementation)
1. :pushpin: [Let’s code the roots of Functional Programming: Lambda calculus implemented in Typescript](https://medium.com/hackernoon/lets-code-the-roots-of-functional-programming-lambda-calculus-implemented-in-typescript-36806ebc2857)
1. :see_no_evil: [Functors and Monads in plain TypeScript](https://dev.to/airtucha/functors-and-monads-in-plain-typescript-33o1)
1. :see_no_evil: [Monads simplified with Generators in TypeScript](https://medium.com/flock-community/monads-simplified-with-generators-in-typescript-part-1-33486bf9d887)
1. :see_no_evil: [Emulating the Lambda Calculus in TypeScript's Type System](https://ayazhafiz.com/articles/21/typescript-type-system-lambda-calculus)


## TypeScript features

1. :bulb: [How to use generics in TypeScript](https://www.digitalocean.com/community/tutorials/how-to-use-generics-in-typescript)
1. :bulb: [TypeScript abstract classes](https://www.tutorialsteacher.com/typescript/abstract-class)


## Techniques

1. :pushpin: [Optimizing JavaScript with Lazy Evaluation and Memoization](https://thoughts.travelperk.com/optimizing-js-with-lazy-evaluation-and-memoization-9d0cd8c30cd4)
1. :pushpin: [JavaScript Lazy Evaluation: Iterables & Iterators](https://javascript.plainenglish.io/javascript-lazy-evaluation-iterables-iterators-e0770a5de96f)


## Books

1. :pushpin: [Becoming a better programmer](https://drive.google.com/file/d/1MuyG49QEjYAdiAMPQSK4I8C-pfqsATao/view?usp=sharing)
1. :bulb: [Design Patterns: Elements of Reusable Object-Oriented Software](https://github.com/muthukumarse/books-1/blob/master/Design%20Patterns%2C%20Elements%20of%20Reusable%20Object-Oriented%20Software.pdf)
