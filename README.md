# Learning Path

This project contains introduction materials, tutorials, guides, resources and other helpful content to a myriad of
topics covered on the work daily basis on MZ, such as:

- Business logic and rules
- Software development
- Infrastructure and DevOps
- Databases and other stateful applications


## Topics

- [TypeScript](typescript.md)
